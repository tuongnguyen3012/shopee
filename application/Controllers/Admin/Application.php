<?php namespace App\Controllers\Admin;

use CodeIgniter\Controller;

class Application extends Controller
{
	public $data = array();
	public function __construct(...$params)
	{
		parent::__construct(...$params);
		$this->data['page_title'] = '';
		helper('cookie');
		if(get_cookie(md5('login'))){
			
		} else {
			$this->my_redirect('/admin/login');
		}
	}

	public function index()
	{
		return view('');
	}
	public function render($view = '')
	{
		$this->data['content'] = view('admin/'.$view, $this->data);
		return view('admin/layout', $this->data);
	}
	public function my_redirect($url){
		header("Location:".base_url($url));exit();
	}

}
