<?php namespace App\Controllers\Admin;

use CodeIgniter\Controller;
use App\Models\ProductModel;
use App\Models\CatModel;

class Category extends Application
{
	public function __construct(...$params){
		parent::__construct(...$params);
	}
	public function index()
	{
		$catmodel = new CatModel();
		$by = 'in_menu';
		if(isset($_GET['by']) && $_GET['by']){
			$by = $_GET['by'];
		}
		$data = [
            'data' => $catmodel->orderBy($by, 'desc')->paginate(100),
            'pager' => $catmodel->pager
		];
		$this->data['results'] = $data;
		return $this->render('category');
	}

	public function edit($id){
		$catmodel = new CatModel();
		$data = $catmodel->find($id);
		if(isset($_POST['display_name'])){
			$data_update['display_name'] = $_POST['display_name'];
			$data_update['in_menu'] = isset($_POST['in_menu']) && $_POST['in_menu'] ? 1 : 0;
			$res = $catmodel->update($id, $data_update);
			print_r($res);
		}
		$this->data['results'] = $data;
		return $this->render('category_edit');
	}

}
