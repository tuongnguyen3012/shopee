<?php namespace App\Controllers\Admin;
use CodeIgniter\Controller;
class Config extends Application
{
	public function __construct(...$params){
		parent::__construct(...$params);
		if(!is_dir(WRITEPATH.'data/'.HTTP_HOST_APP))
		{
			mkdir(WRITEPATH.'data/'.HTTP_HOST_APP, 0777, true);
		}
	}
	public function index()
	{
		if($this->request->getMethod() == 'post')
		{
			$configs = $this->request->getPost();
			$configs = $configs['config'];
			file_put_contents(WRITEPATH.'data/'.HTTP_HOST_APP.'/config.json', json_encode($configs));
			return redirect(base_url('admin/config/'));
		}
		if(file_exists(WRITEPATH.'data/'.HTTP_HOST_APP.'/config.json'))
		{
			$configs = @json_decode(file_get_contents(WRITEPATH.'data/'.HTTP_HOST_APP.'/config.json'));
		}
		if(isset($configs))
			$this->data['configs'] = $configs;
		return $this->render('config');
	}
	public function menu(){
		if($this->request->getMethod() == 'post')
		{
			$configs = $this->request->getPost();
			$configs = $configs['config'];
			$hotkeys = explode("\n",$configs['hotkey']);
			$homemenu = explode("\n",$configs['homemenu']);
			$sets['hotkeys'] = $hotkeys;
			$sets['homemenu'] = $homemenu;
			file_put_contents(WRITEPATH.'data/'.HTTP_HOST_APP.'/menu_hot_keys.json', json_encode($sets));
			return redirect(base_url('admin/menu/'));
		}
		if(file_exists(WRITEPATH.'data/'.HTTP_HOST_APP.'/menu_hot_keys.json'))
		{
			$configs = @json_decode(file_get_contents(WRITEPATH.'data/'.HTTP_HOST_APP.'/menu_hot_keys.json'));
		}
		if(isset($configs)){
			$this->data['configs'] = $configs;
		}
		return $this->render('menu');
	}
}
