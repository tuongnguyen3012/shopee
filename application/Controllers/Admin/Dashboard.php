<?php namespace App\Controllers\Admin;

use CodeIgniter\Controller;
use App\Models\ProductModel;

class Dashboard extends Application
{
	public function __construct(...$params){
		parent::__construct(...$params);
	}
	public function index()
	{
		if(isset($_GET['truncate']) && $_GET['table'] == 'product'){
			$productmodel = new ProductModel();
			$data = $productmodel->product_table->truncate();
		}
		return $this->render('dashboard');
	}

}
