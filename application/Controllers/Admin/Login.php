<?php namespace App\Controllers\Admin;

use CodeIgniter\Controller;

class Login extends Controller
{
	public $data = array();
	public function __construct(...$params)
	{
		parent::__construct(...$params);
		$this->data['page_title'] = '';
		helper('cookie');
	}
	public function index()
	{
		if(get_cookie(md5('login'))){
			$this->my_redirect('/admin');
		} else {
			if(isset($_GET['admin']) && $_GET['admin'] == 'tuong'){
				set_cookie(md5('login'), md5('tuong_login'), 60*60*24*2);
				echo 'Chúc mừng tưởng. Bạn đã đăng nhập'.' <a href="/admin">ADMIN</a>';
			} else {
				echo "Vui lòng đăng nhập, để có thể là một người thích làm gì cũng được.";
			}
		}
	}
	public function my_redirect($url){
		header("Location:".base_url($url));exit();
	}
}
