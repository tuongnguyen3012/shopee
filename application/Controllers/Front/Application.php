<?php namespace App\Controllers\Front;

use App\Models\CatModel;
use CodeIgniter\Controller;

class Application extends Controller
{
    public $data = array();
    public function __construct(...$params)
    {
        parent::__construct(...$params);
        $this->data['page_title'] = META_TITLE;
        $this->data['page_description'] = META_DESCRIPTION;
        $this->data['page_image'] = '';
        helper('general');
    }

    public function index()
    {
        return view('');
    }
    public function login()
    {
        return view('xxsd');
    }
    public function render($view = '')
    {
        $cats = new CatModel();
        $this->data['menu'] = bootstrap_menu($cats->getMenu());
        $this->data['content'] = view('front/'.$view, $this->data);
        return view('front/layout', $this->data);
    }
    //--------------------------------------------------------------------

}
