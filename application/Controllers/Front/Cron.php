<?php namespace App\Controllers\Front;

use App\Models\CatModel;
use App\Models\ProductModel;

class Cron extends Application
{
    public function __construct(...$params)
    {
        parent::__construct(...$params);
        $this->catmodel = new CatModel();
        $this->productmodel = new ProductModel();
        $this->shopeelib = new \App\Libraries\Shopee();
    }
    public function getProduct($shopid, $itemid)
    {
        $catlist = $this->shopeelib->getProduct($shopid, $itemid);
        echo '<pre>';
        print_r($catlist);
        echo '</pre>';
    }
    public function gencat()
    {
        $catlist = $this->shopeelib->getCats();
        foreach($catlist as $cat)
        {
            if(isset($cat->main))
            {
                if(!$this->catmodel->getByIdShopee($cat->main->catid))
                {
                    $this->catmodel->AddCat((array)$cat->main);
                }
            }
            if(isset($cat->sub))
            {
                foreach($cat->sub as $sub) {
                    if (!$this->catmodel->getByIdShopee($sub->catid)) {
                        $this->catmodel->AddCat(array(
                                'display_name' => $sub->display_name,
                                'name' => $sub->name,
                                'catid' => $sub->catid,
                                'image' => $sub->image,
                                'parent_category' => $sub->parent_category,
                                'is_adult' => $sub->is_adult,
                                'sort_weight' => $sub->sort_weight,
                            )
                        );
                    }
                    if(isset($sub->sub_sub))
                    {
                        foreach($sub->sub_sub as $sub_sub) {
                            if (!$this->catmodel->getByIdShopee($sub_sub->catid)) {
                                $this->catmodel->AddCat(array(
                                        'display_name' => $sub_sub->display_name,
                                        'name' => $sub_sub->name,
                                        'catid' => $sub_sub->catid,
                                    )
                                );
                            }
                        }
                    }
                }
            }
        }
        echo 'xong';
    }
    //gen cate for sexy fr
    public function gencatsexyfr()
    {
        $catlist = $this->shopeelib->getCats();
        foreach($catlist as $cat)
        {
            if(isset($cat->main))
            {
                if(!$this->catmodel->getByIdShopee($cat->main->catid))
                {
                    $main = $cat->main;
                    $this->catmodel->AddCat(
                        array(
                            'name' => $main->display_name,
                            'slug' => changeTitle($main->display_name),
                            'collectionid' => $main->catid,
                            'image' => $main->image,
                            'parent_id' => $main->parent_category
                        )
                    );
                }
            }
            if(isset($cat->sub))
            {
                foreach($cat->sub as $sub) {
                    if (!$this->catmodel->getByIdShopee($sub->catid)) {
                        $this->catmodel->AddCat(array(
                                'name' => $sub->display_name,
                                'slug' => changeTitle($sub->display_name),
                                'collectionid' => $sub->catid,
                                'image' => $sub->image,
                                'parent_id' => $sub->parent_category
                            )
                        );
                    }
                    $parent_sub_sub = $sub->catid;
                    if(isset($sub->sub_sub))
                    {
                        foreach($sub->sub_sub as $sub_sub) {
                            if (!$this->catmodel->getByIdShopee($sub_sub->catid)) {
                                $this->catmodel->AddCat(array(
                                        'name' => $sub_sub->display_name,
                                        'slug' => changeTitle($sub_sub->display_name),
                                        'collectionid' => $sub_sub->catid,
                                        'image' => $sub_sub->image,
                                        'parent_id' => $parent_sub_sub,
                                    )
                                );
                            }
                        }
                    }
                }
            }
        }
        echo 'xong';
    }
    public function fixitemid(){
        if(isset($_GET['tuong'])){
            $productmodel = new ProductModel();
            $itemid = 2147483647;
            $limit = isset($_GET['limit']) ? $_GET['limit'] : 5;
            $data = $productmodel->product_table->where('itemid', $itemid)->orderBy('updated_at', 'asc')->limit($limit, 0)->get()->getResult('array');;
            // print_r($data); die;
            if($data){
                foreach($data as $item){
                    $id = $item['id'];
                    $data_item = json_decode($item['data_json'], true);
                    $data_update = array(
                        'updated_at' => date('Y-m-d H:i:s'),
                    );
                    if(isset($data_item['itemid']) && $data_item['itemid']){
                        $data_update['itemid'] = $data_item['itemid'];
                        $data_update['shopid'] = $data_item['shopid'];
                    }
                    $query = $productmodel->product_table->where('id', $id)->set($data_update)->update();
                    if($query){echo 'thành công';}else{echo 'thất bại';}
                }
            }
            
        } else {
            die('xxxx');
        }
    }

}
