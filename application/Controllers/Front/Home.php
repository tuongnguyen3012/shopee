<?php namespace App\Controllers\Front;

use App\Libraries\Shopee;
use App\Models\CatModel;
use App\Models\ProductModel;

class Home extends Application
{
    public function __construct(...$params)
    {
        parent::__construct(...$params);
        $this->shopee = new Shopee();
        if(file_exists(WRITEPATH.'data/'.$_SERVER['HTTP_HOST'].'/menu_hot_keys.json'))
        {
            $this->data['menu_hot_keys'] = @json_decode(file_get_contents(WRITEPATH.'data/'.$_SERVER['HTTP_HOST'].'/menu_hot_keys.json'), true);
        }
    }

    public function index()
    {
        $this->data['items'] = [];
        if(!isset($_GET['clearcache'])){
            if(file_exists(WRITEPATH.'data/'.$_SERVER['HTTP_HOST'].'/home_data.json')){
                $res = @json_decode(file_get_contents(WRITEPATH.'data/'.HTTP_HOST_APP.'/home_data.json'));
                //cache home trong vòng 1 TUẦN, tự reset khi sang ngày sau
                if(isset($res->created_at)&& isset($res->items) && $res->items){
                    $next_day = date('Y-m-d', strtotime(date("Y-m-d", strtotime($res->created_at)) . " +1 week"));
                    if(($next_day >= date('Y-m-d')) ){
                        $this->data['items'] = $res->items;
                    }
                }
            } 
        }
        
        if(!isset($this->data['items']) || empty($this->data['items'])) {
            $data_items = [];
            if(isset($this->data['menu_hot_keys']['homemenu'])&&$this->data['menu_hot_keys']['homemenu']){
                $keyword = $this->data['menu_hot_keys']['homemenu'][0];
                $limit = 48;
                $params = [
                    'keyword' => $keyword,
                    'limit' => $limit
                ];
                $data = $this->shopee->searchKeyword($params);
                if(isset($data->items)){
                    $data_items = $data->items;
                }
            } else {
                $productsrecommend = $this->shopee->getRecommend();
                if(isset($productsrecommend->data->items)){
                    $data_items = $productsrecommend->data->items;
                }
            }
            
            if($data_items){
                $this->data['items'] = $data_items;
                $json['items'] = $this->data['items'];
                $json['created_at'] = date('Y-m-d');
                file_put_contents(WRITEPATH.'data/'.HTTP_HOST_APP.'/home_data.json', json_encode($json));
            }
        }

        $this->data['categries'] = [];
        if(file_exists(WRITEPATH.'data/'.$_SERVER['HTTP_HOST'].'/menu_category.json'))
        {
            $this->data['categries'] = @json_decode(file_get_contents(WRITEPATH.'data/'.$_SERVER['HTTP_HOST'].'/menu_category.json'), true);
        }
        $this->data['page_title'] = META_TITLE;
        $this->data['page_description'] = META_DESCRIPTION;
        return $this->render('home');
    }
    public function product($shopid = null, $itemid = null)
    {
        $productmodel = new ProductModel();
        if($cacheproduct = $productmodel->getByIdShopee($itemid, $shopid))
        {
            $product = json_decode($cacheproduct['data_json']);
            //time > 1 month thì reset product data
            $current_date = time();
            $date_date_month_later = strtotime(date( "Y-m-d H:i:s", strtotime( date($cacheproduct['updated_at']) ) ) . "+1 month" );
            if($date_date_month_later <= $current_date){
                $id = $cacheproduct['id'];
                $product_tmp = $this->shopee->getProduct($shopid, $itemid);
                $product_tmp = $product_tmp->item;
                if($product_tmp->price)
                {
                    $data = array(
                        'updated_at' => date('Y-m-d H:i:s'),
                        'data_json' => json_encode($product_tmp),
                    );
                    $productmodel->product_table->where('id', $id)->set($data)->update();
                }
            }
        }
        else
        {
            $product = $this->shopee->getProduct($shopid, $itemid);
            $product = $product->item;
            if($product && $product->price)
            {
                $data = array(
                    'itemid' => $itemid,
                    'shopid' => $shopid,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'data_json' => json_encode($product),
                );
                $productmodel->product_table->insert($data, true);
            }
        }
        if(!isset($product->itemid))
        {
            return $this->render('404');
        }
        $categorymodel = new CatModel();
        $this->data['product'] = $product;
        $catlist = array();
        if(isset($product->catid))
        {
            $catlist[] = $product->catid;
        }
        if(isset($product->sub_catid))
        {
            $catlist[] = $product->sub_catid;
        }
        if(isset($product->third_catid)) {
            $catlist[] = $product->third_catid;
        }
        if(isset($product->categories) && $product->categories) {
            foreach($product->categories as $category){
                $catlist[] = $category->catid;
            }
        }
        $catlist = array_unique($catlist);
        $breadcrumbs = array();
        $category = $categorymodel->findWhere(array('catid IN ('.implode(',', $catlist).')' => null));
        foreach($category as $cat)
        {
            $breadcrumbs[] = array(
                'name' => $cat['display_name'],
                'url' => cat_url($cat['display_name'], $cat['catid']),
            );
        }
        if(isset($catlist[0])){
            $product_relateds = $this->shopee->getRecommend($catlist[0], 6, 33 , 0);
            if(isset($product_relateds->data->items)){
                $this->data['product_relateds'] = $product_relateds->data->items;
            }
        }
        $this->data['dupplicate_key'] = detect_dupplicate_text($product->description);
        $shop_detail = $this->shopee->shop_detail($shopid);
        $this->data['shop_detail'] = isset($shop_detail->data) ? $shop_detail->data : [];
        $this->data['breadcrumbs'] = $breadcrumbs;
        $this->data['page_title'] = name_shopee($product->name);
        $this->data['page_description'] = meta_des($product->name);
        $this->data['page_image'] = image_shopee($product->image);
        return $this->render('product');
    }
    public function category($cate_name, $catid)
    {
        $page = 1;
        if(isset($_GET['page']) && is_numeric($_GET['page']) && $_GET['page'] > 0){
            $page = $_GET['page'];
        }
        $limit = 60;
        $set = ($page - 1) * $limit;
        $cats = new CatModel();
        $category = $cats->findWhere(['catid' => $catid]);
        if(empty($category)){
            return $this->render('404');
        }
        $category = $category[0];
        $this->data['sort'] = 'sales_desc';
        $by = 'sales';
        $order = 'desc';

        //ALTER TABLE `categories` ADD `results` MEDIUMTEXT NULL AFTER `image`;
        $results = [];
        $data_update = [];
        if($page == 1){
            $current_date = time();
            $result_next_week = strtotime(date( "Y-m-d H:i:s", strtotime($category['created_at'])) . "+1 week" );
            if($result_next_week >= $current_date){
                $results = json_decode($category['results']);
                unset($category['results']);
            }
        }
        $current_base_cate = current_url();
        if(isset($_GET['sort']) && !empty($_GET['sort'])){
            if(sortConvertText($_GET['sort'])){
                $valueSort = sortConvertText($_GET['sort'])['value'];
                $this->data['sort'] = $_GET['sort'];
                $by = $valueSort['by'];
                $order = $valueSort['order'];
                $current_base_cate .= '?sort='.$this->data['sort'];
            }
            if($page == 1 && $_GET['sort'] == 'sales_desc' && $results && $results->type == 'searchItem'){
                $this->data['items'] = $results->items;
            } else {
                $productsrecommend = $this->shopee->searchItem('', $catid, $limit, $set, $by, $order);
                if(isset($productsrecommend->items) && $productsrecommend->items)
                {
                    $this->data['items'] = $productsrecommend->items;
                    if($page == 1 && $_GET['sort'] == 'sales_desc' && !$results && $this->data['items']){
                        $data_update['results'] = json_encode(['type' => 'searchItem', 'items' =>$this->data['items']]);
                    }
                }
            }
        } else {
            if($results){
                $this->data['items'] = $results->items;
            } else {
                //category lever 1
                if(is_numeric($category['parent_category']) && $category['parent_category'] == 0){
                    $productsrecommend = $this->shopee->getRecommend($catid, 6, $limit , $set);
                    if(isset($productsrecommend->data->items) && $productsrecommend->data->items){
                        $this->data['items'] = $productsrecommend->data->items;
                        if($page == 1){
                            $data_update['results'] = json_encode(['items' => $this->data['items'], 'type' => 'recommend']);
                        }
                    }
                } else {
                ////category lever > 1
                    $productsrecommend = $this->shopee->searchItem('', $catid, $limit, $set, $by, $order);
                    if(isset($productsrecommend->items) && $productsrecommend->items)
                    {
                        $this->data['items'] = $productsrecommend->items;
                        if($page == 1){
                            $data_update['results'] = json_encode(['items' => $this->data['items'], 'type' => 'searchItem']);
                        }
                    }
                }
            }
        }
        if(!empty($category['tags'])){
            $this->data['key_suggest'] = json_decode($category['tags'], true);
        } else {
            $this->data['key_suggest'] = getKeysuggest($category['display_name']);
            $data_update['tags'] = json_encode($this->data['key_suggest']);
        }
        
        if($data_update){
            if(isset($data_update['results']) && $data_update['results']){
                $data_update['created_at'] = date('Y-m-d H:i:s');
            }
            $cats->updateCategory(['id'=>$category['id']], $data_update);
        }
        
        $display_name = $category ? $category['display_name']: '';
        $cate_name = str_replace('-', ' ', urldecode($cate_name));
        $this->data['limit'] = $limit;
        $this->data['sub_category'] = $cats->findWhere(array('parent_category' => $catid));
        $this->data['category'] = $category ? $category : [];
        $this->data['cate_name'] = !empty($display_name) ? $display_name : $cate_name;
        $prefix_page = (isset($_GET['page']) && $_GET['page'] ? ' - Trang '. $_GET['page'] : '');
        $this->data['page_title'] = !empty($display_name) ? $display_name . $prefix_page : $cate_name;
        $this->data['page_description'] = $category ? meta_des($display_name).$prefix_page: '';

        $prefix_query = (strpos($current_base_cate, '?') !== false) ? '&' : '?'; 
        $this->data['prev_link'] = ($page - 1 > 1) ? $current_base_cate.$prefix_query.'page='.($page - 1) : $current_base_cate;
        $this->data['current_link'] = '';
        $this->data['next_link'] = $current_base_cate.$prefix_query.'page='.($page+1);
        return $this->render('category');
    }

    public function search(){
        if(isset($_GET['q']) && $_GET['q']){
            $keyword = $_GET['q'];
            $page = 1;
            if(isset($_GET['page']) && $_GET['page']){
                $page = $_GET['page'];
            }
            $limit = 48;
            $offset = ($page - 1) * $limit;
            $params = [
                'keyword' => $keyword,
                'limit' => $limit,
                'offset' => $offset,
                'by' => "sales",
                'order' => "desc"
            ];
            $this->data['sort'] = 'sales_desc';
            if(isset($_GET['sort']) && !empty($_GET['sort'])){
                //?by=ctime&order=desc //Mới nhất
                //?by=pop&order=desc //Phổ biến
                //?by=sales&order=desc //Bán Chạy
                //?by=price&order=asc //Giá thấp đến cao
                //?by=price&order=desc //Giá cao đến thấp
                //?by=relevancy&order=desc //mức độ liên quan -> cái này dành cho search
                $this->data['current_url'] = current_url();
                if(sortConvertText($_GET['sort'])){
                    $valueSort = sortConvertText($_GET['sort'])['value'];
                    $this->data['sort'] = $_GET['sort'];
                    $by = $valueSort['by'];
                    $order = $valueSort['order'];
                    $params['by'] = $by;
                    $params['order'] = $order;
                }
            }
            $data = $this->shopee->searchKeyword($params);
            $this->data['limit'] = $limit;
            $this->data['cate_name'] = $keyword;
            $prefix_page = (isset($_GET['page']) && $_GET['page'] ? ' - Trang '. $_GET['page'] : '');
            $this->data['page_title'] = $keyword ? $keyword .  $prefix_page : '';
            $this->data['page_description'] = $keyword ? meta_des($keyword). $prefix_page : '';
            $this->data['items'] = isset($data->items) ? $data->items : [];
            $this->data['url_search'] = search_url($keyword);
            $this->data['prev_link'] = search_url($keyword).'&page='.($page - 1 > 1 ? $page - 1 : 1);
            $this->data['current_link'] = search_url($keyword).'&page='.$page;
            $this->data['next_link'] = search_url($keyword).'&page='.($page+1);
            $this->data['key_suggest'] = getKeysuggest($keyword);
            return $this->render('category');
        }
    }

    public function subsitemap($slug, $id)
    {
        if(file_exists(WRITEPATH.'data/'.$_SERVER['HTTP_HOST'].'/'.$slug.'.xml'))
        {
            $content = file_get_contents(WRITEPATH.'data/'.$_SERVER['HTTP_HOST'].'/'.$slug.'.xml');
        }
        else
        {
            $productsrecommend = $this->shopee->getRecommend($id, $recommend_type = 6, 60 , 0);
            if(isset($productsrecommend->data->items))
            {
                $this->data['items'] = $productsrecommend->data->items;
            }
            $content = view('front/subsitemap', $this->data);
            file_put_contents(WRITEPATH.'data/'.$_SERVER['HTTP_HOST'].'/'.$slug.'.xml',$content);
        }
        $this->response->setHeader('Content-Type', 'text/xml;charset=utf-8');
        $this->response->setBody($content);
        $this->response->send();
        return $this;
    }

    public function sitemap()
    {
        $page = 1;
        if(isset($_GET['page']) && is_numeric($_GET['page']) && $_GET['page'] > -1){
            $page = $_GET['page'];
        }
        $limit = 60;
        $start = ($page - 1) * $limit;
        $cats = new CatModel();
        $params = ['limit' => $limit, 'start' => $start];
        if($page > 1){
            $params['not_check_parent'] = 1;
        }
        $data_cate = $cats->getlist($params);
        if($data_cate){
            $this->data['data_cate'] = $data_cate;
        }
        $this->response->setHeader('Content-Type', 'text/xml;charset=utf-8');
        $this->response->setBody(view('front/sitemap', $this->data));
        $this->response->send();
        return $this;
    }

    public function shop($name='', $shopid){
        if($shopid){
            $shop_detail = $this->shopee->shop_detail($shopid);
            if(!isset($shop_detail->data))
            {
                return $this->render('404');
            }
            $shop_data = $shop_detail->data;
            $page = 1;
            if(isset($_GET['page']) && $_GET['page']){
                $page = $_GET['page'];
            }
            $limit = 48;
            $offset = ($page - 1) * $limit;
            $params = [
                'match_id' => $shopid,
                'limit' => $limit,
                'offset' => $offset,
                'by' => "pop",
                'order' => "desc"
            ];
            $this->data['sort'] = 'pop_desc';
            if(isset($_GET['sort']) && !empty($_GET['sort'])){
                $this->data['current_url'] = current_url();
                if(sortConvertText($_GET['sort'])){
                    $valueSort = sortConvertText($_GET['sort'])['value'];
                    $this->data['sort'] = $_GET['sort'];
                    $by = $valueSort['by'];
                    $order = $valueSort['order'];
                    $params['by'] = $by;
                    $params['order'] = $order;
                }
            }
            $data = $this->shopee->shop_product($params);
            $this->data['limit'] = $limit;
            $this->data['cate_name'] = $shop_data->name;
            $prefixshop = ['cua-hang' => 'Cửa Hàng', 'shop' => 'Shop', 'store' => 'Store'];
            $prefix_page = (isset($_GET['page']) && $_GET['page'] ? ' - Trang '. $_GET['page'] : '');
            $this->data['page_title'] = $shop_data->name ? $shop_data->name.' - '.$prefixshop[PREFIX_SHOP].' online tại '.$shop_data->shop_location .$prefix_page: '';
            $this->data['page_description'] = isset($shop_data->description) ? strip_tags($shop_data->description) : $shop_data->name.' - '.$shop_data->place;
            $this->data['page_description'] .= $prefix_page;
            $this->data['short_description'] = isset($shop_data->description) ? strip_tags($shop_data->description) : '';
            $this->data['page_image'] = isset($shop_data->shop_covers[0]->image_url) ? image_shopee($shop_data->shop_covers[0]->image_url) : '';
            $this->data['items'] = isset($data->items) ? $data->items : [];
            $this->data['prev_link'] = shop_url($name, $shopid).'?page='.($page - 1 > 1 ? $page - 1 : 1);
            $this->data['current_link'] = shop_url($name, $shopid).'?page='.$page;
            $this->data['next_link'] = shop_url($name, $shopid).'?page='.($page+1);
            return $this->render('category');
        }
    }

    public function test()
    {
        $jsonStr = file_get_contents("php://input"); //read the HTTP body.
        $json = json_decode($jsonStr);
        echo '<pre>';
        print_r($json);
        echo '</pre>';
        echo '<pre>';
        print_r($_POST);
        echo '</pre>';
    }
    //--------------------------------------------------------------------

}
