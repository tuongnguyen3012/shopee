<?php
function my_ucwords($page_title){
    return mb_convert_case(mb_strtolower($page_title), MB_CASE_TITLE, "UTF-8");
}
function image_shopee($image, $type = '')
{
    if($type)
    {
        if(file_exists('https://cf.shopee.vn/file/'.$image.'_'.$type)){
            return 'https://cf.shopee.vn/file/'.$image.'_'.$type;
        }
        return 'https://cf.shopee.vn/file/'.$image;
    }
    else
    {
        return 'https://cf.shopee.vn/file/'.$image;
    }
}
function product_url($shopid, $itemid, $name)
{
    return base_url(PREFIX_PRODUCT.'/'.url_title($name).'-i.'.$shopid.'.'.$itemid);
}
function name_shopee($name)
{
    return $name;
}
function meta_des($name){
    return "Sở hữu ngay ".$name." với giá siêu rẻ nhưng chất lượng vẫn cực kỳ tốt, cửa hàng uy tín, được đảm bảo bởi Shopee, giao hàng toàn quốc và được Miễn Phí ship... XEM NGAY!";
}
function price_shopee($price)
{
    $priceFloat = $price/100000;
    $symbol = 'đ';
    $symbol_thousand = '.';
    $decimal_place = 0;
    $price = number_format($priceFloat, $decimal_place, '', $symbol_thousand);
    return $price.$symbol;
}
function discount_shopee($discount = 0)
{
    return $discount;
}
function cat_url($name, $catid, $keep = false){
    $url_title = 'category';
    if($keep){
        $url_title = $name;
    } else {
        $url_title = url_title($name);
    }
    return base_url(PREFIX_CATEGORY.'/'.$url_title.'-c.'.$catid);
}
function shop_url($name='', $shopid){
    if($name){
        $url_title = $name;
    } else {
        $url_title = 'shop';
    }
    return base_url(PREFIX_SHOP.'/'.$url_title.'-'.$shopid);
}
function sitemap_url($name, $catid){
    return base_url('/sitemap/'.url_title($name).'-c.'.$catid.'.xml');
}
function affilate_link($name, $shopid, $itemid)
{
    $AFFILIATE_ID = 4868622623283709067;
    return 'https://fast.accesstrade.com.vn/deep_link/'.$AFFILIATE_ID.'?url=https://shopee.vn/'.url_title($name).'-i.'.$shopid.'.'.$itemid;
    // if(env('AFFILIATE_ID'))
    // {
    //     return 'https://fast.accesstrade.com.vn/deep_link/'.env('AFFILIATE_ID').'?url=https://shopee.vn/'.url_title($name).'-i.'.$shopid.'.'.$itemid;
    // }
    // else
    // {
    //     return 'https://fast.accesstrade.com.vn/deep_link/4530993072167178991?url=https://shopee.vn/'.url_title($name).'-i.'.$shopid.'.'.$itemid;
    // }
}
function search_url($q){
    return site_url(PREFIX_SEARCH).'?q='.urlencode($q);
}
//menu html
function bootstrap_menu($array,$parent_id = 0,$parents = array())
{
    if($parent_id==0)
    {
        foreach ($array as $element) {
            if (($element['parent_category'] != 0) && !in_array($element['parent_category'],$parents)) {
                $parents[] = $element['parent_category'];
            }
        }
    }
    $menu_html = '';
    foreach($array as $element)
    {
        if($element['parent_category']==$parent_id)
        {
            $class = "";
            if(in_array($element['catid'],$parents))
            {
                $menu_html .= '<li class="dropdown '.$class.'">';
                $menu_html .= '<a title="' . $element['display_name'] . '" href="'.cat_url(trim($element['display_name']), $element['catid']).'" class="abc dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">'.$element['display_name'].' <span class="caret"></span></a>';
            }
            else {
                $menu_html .= '<li class="'.$class.'"">';
                $menu_html .= '<a title="' . $element['name'] . '" href="' . cat_url(trim($element['display_name']), $element['catid']) . '">' . $element['display_name'] . '</a>';
            }
            if(in_array($element['catid'],$parents))
            {
                $menu_html .= '<ul class="dropdown-menu" role="menu">';
                $menu_html .= bootstrap_menu($array, $element['catid'], $parents);
                $menu_html .= '</ul>';
            }
            $menu_html .= '</li>';
        }
    }
    return $menu_html;
}

function sortConvertText($text){
    //?by=ctime&order=desc //Mới nhất
    //?by=pop&order=desc //Phổ biến
    //?by=sales&order=desc //Bán Chạy
    //?by=price&order=asc //Giá thấp đến cao
    //?by=price&order=desc //Giá cao đến thấp
    //?by=relevancy&order=desc //mức độ liên quan -> cái này dành cho search
    $array = [
        'ctime_desc' => 'Mới nhất',
        'pop_desc' => 'Phổ biến',
        'sales_desc' => 'Bán chạy',
        'price_asc' => 'Giá thấp - cao',
        'price_desc' => 'Giá cao - thấp',
        'relevancy_desc' => 'Liên quan'
    ];
    if(isset($array[$text])){
        $sortValue = explode('_', $text);
        return [
            'text' => $array[$text],
            'value' => [
                'by' => $sortValue[0],
                'order' => $sortValue[1]
            ]
        ];
    }
    return false;
}
function detect_dupplicate_text($str){
    // $str = 'Love a and peace rồi love a and peace rồiiii love a and peace rồii love a and peace ccd dda aac love aac a and aac peace love a and peace';
    $str = mb_strtolower($str, 'UTF-8');
    // Build a histogram mapping words to occurrence counts
    $hist = array();
    // Split on any number of consecutive whitespace characters
    $save = [];
    $list = preg_split('/\s+/', $str);
    $tmp_word = '';
    foreach ($list as $key => $word)
    {
        if (isset($hist[$word]))
        {
            $hist[$word]++;

            $tmp_word = trim($tmp_word);
            $count_tmp_word = get_num_of_words($tmp_word);
            $word_comp = '';
            if($count_tmp_word > 1){
                for($i=$count_tmp_word;$i>=0;$i--){
                    $word_comp .= ' '.$list[$key-$i];
                }
                $word_comp = trim($word_comp);
                if($tmp_word == $word_comp){
                    $tmp_word .= ' '.$word;
                }
            } else {
                if(!empty($word)) $tmp_word .= ' '.$word;
            }

            //loại bỏ những từ đã tồn tại trong mảng lặp, nhưng không ở phía trước từ đó

        }
        else
        {
            $tmp_word = RemoveSpecialChapr($tmp_word);
            // echo $tmp_word;
            $tmp_word = trim($tmp_word);
            // $tmp_word = mb_trim($tmp_word,"và");
            if(!empty($tmp_word) && !is_numeric($tmp_word) && !in_array($tmp_word, $save) && get_num_of_words($tmp_word) > 1){//
                array_push($save, $tmp_word);
            }
            $tmp_word = '';
            // $hist[$tmp_word] = 1;
            $hist[$word] = 1;
        }
    }
    $save = array_unique($save);
    return $save;
}
function mb_trim($string, $trim_chars = '\s'){
    return preg_replace('/^['.$trim_chars.']*(?U)(.*)['.$trim_chars.']*$/u', '\\1',$string);
}
function checkRemoveWord($word = ''){
    $list_remove = ['là', 'thì', 'không', 'phải', 'bị', 'với', 'ở', 'sử', 'trên', 'dưới', 'trái', 'phải', 'giữa', 'xuống', 'từ', 'sản phẩm'];
    if(in_array($word, $list_remove)){
        return false;
    }
    return true;
}
function RemoveSpecialChapr($value){
    $title = str_replace( array( '\'', '', '"', ',' , ';', '<', '>', '.', '-', ')', '(', ':', '.', '/', '❤' ), ' ', $value);
    return $title;
}
//build slug
function stripUnicode($str)
{
    if(!$str) return false;
    $unicode= array(
        'a' => 'á|à|ả|ã|ạ|ă|ắ|ẳ|ằ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ',
        'A' => 'Á|À|Ả|Ã|Ạ|Ă|Ắ|Ằ|Ẳ|Ẵ|Ặ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ',
        'd' => 'đ',
        'D' => 'Đ|Ð',
        'e' => 'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ễ|ệ|ể',
        'E' => 'É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ễ|Ệ|Ể',
        'i' => 'í|ì|ỉ|ĩ|ị',
        'I' => 'Í|Ì|Ỉ|Ĩ|Ị',
        'o' => 'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ',
        'O' => 'Ó|Ò|Ỏ|Õ|Ọ|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ',
        'u' => 'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự',
        'U' => 'Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự',
        'y' => 'ý|ỳ|ỷ|ỹ|ỵ',
        'Y' => 'Ý|Ỳ|Ỷ|Ỹ|Ỵ'

    );
    foreach ($unicode as $khongdau => $codau) {
        $arr= explode("|", $codau);
        $str= str_replace($arr, $khongdau, $str);
    }
    return $str;
}

function changeTitle($str)
{
    $str= trim($str);
    if($str=="") return "";
    $str= str_replace('"', '', $str);
    $str= str_replace("'", '', $str);
    $str= stripUnicode($str);
    $str= strtolower($str);
    $str= str_replace(' ','-', $str);
    $temp=NULL;
    for($i=50; $i>=1; $i--) $temp[]=str_repeat('-', $i);
    $str= str_replace($temp, '-', $str);
    $str= preg_replace('/[^a-z0-9-]+/i', '', $str);
    if(substr($str, -1) == '-') $str= substr($str, 0, -1);
    return $str;
}
function get_num_of_words($string) {
    $string = preg_replace('/\s+/', ' ', trim($string));
    $words = explode(" ", $string);
    return count($words);
}

if ( ! function_exists('xuly_cookie')) {
    function xuly_cookie(&$cookie)
    {
        $re = '/\[(.*)\]/';
        preg_match($re,$cookie,$match);
        if(isset($match[1])){
            $cookie= json_decode($cookie);
            if($cookie)
            {
                $txt='';
                foreach($cookie as $key => $value){
                    $txt.=$value->name.'='.$value->value.';';
                }
                $cookie = $txt;
                return $txt;
            }
        }
        return $cookie;
    }
}
function curl_get_mobile($url, $cookie = null){
    $browser="Mozilla/5.0 (Linux; Android 4.1.1; Nexus 7 Build/JRO03D) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.166  Safari/535.19";
    // open a site with cookies
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    if($cookie)
    {
        if(!is_array($cookie))
        {
            xuly_cookie($cookie);
            if($cookie!='') curl_setopt($ch, CURLOPT_COOKIE,$cookie);
        }
        else
        {
            curl_setopt ($ch, CURLOPT_COOKIESESSION, TRUE);
            curl_setopt ($ch, CURLOPT_COOKIEJAR, $cookie['file']);
            curl_setopt ($ch, CURLOPT_COOKIEFILE, $cookie['file']);
        }
    }
    curl_setopt($ch, CURLOPT_ENCODING, '');
    curl_setopt($ch, CURLOPT_USERAGENT,$browser);
    curl_setopt($ch, CURLOPT_HEADER  ,0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER  ,1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION  ,1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    $result = curl_exec($ch);
     $infor = curl_getinfo($ch);
    curl_close($ch);
    return $result;
}
function getKeysuggest($key, $google = true)
{
    $key = mb_substr($key, 0, 15, "utf-8");
    if($google){
        $url = 'https://suggestqueries.google.com/complete/search?jsonp=suggestCallBack&q='.urlencode($key).'&client=youtube&ds=yt&_=1527238858171';
    } else {
        $url = 'https://clients1.google.com/complete/search?client=youtube-reduced&hl=fr&gs_rn=64&gs_ri=youtube-reduced&ds=yt&cp=3&gs_id=h&q='.urlencode($key).'&gs_gbg=Lz4H70KrfRoq06Ya6N';
    }
    $results = curl_get_mobile($url);
    preg_match_all("/\"([^\"]*)\"/", $results, $keys);
    if(strpos($results, '<head>'))
    {
        return array();
    }
    $listkey = array();
    if(isset($keys[1]) && $keys[1])
    {
        foreach($keys[1] as $key)
        {
            if(strlen(trim($key)) < 2)
            {
                break;
            }
            $listkey[] = mb_strtolower($key);
        }
    }
    $listkey = array_unique($listkey);
    return $listkey;
}