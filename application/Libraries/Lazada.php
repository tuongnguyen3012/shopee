<?php namespace App\Libraries;


use Config\App;
use Config\Services;

/**
 * Class General
 *
 * @package App\Libraries
 */
class Lazada
{
    public function __construct()
    {
    }
    public function getforyou($page)
    {
        $body = curl_get('http://overseas-aladdin.alicdn.com/bottom/icms-zebra-5000379-2586240/language=vi/pageNo='.$page.'/platform=pc/regionID=VN/data.jsonp?callback=callback_icms_zebra_5000379_2586240_language_vi_pageNo_'.(int)$page.'_platform_pc_regionID_VN', '', array(), 10, false);
        $strdata = trim(substr($body, strpos($body, '('), strlen($body)), '()');
        $data = json_decode($strdata);
        return $data;
    }
    public function getbyslug($slug, $page = 1)
    {
        $url = 'https://www.lazada.vn/'.$slug.'/?ajax=true&page='.$page;
        $results = curl_get($url);
        $list = json_decode($results);
        return $list;
    }
    public function search($key, $page = 1)
    {
        $url = 'https://www.lazada.vn/catalog/?ajax=true&page='.$page.'&q='.urlencode($key);
        $results = curl_get($url);
        $list = json_decode($results);
        return $list;
    }
    protected function genProduct($body)
    {
        $product = array();
        preg_match("/property=\"og:title\" content=\"([^\"]*)\"/", $body, $title);
        if($title)
        $product['title'] = @$title[1];
        preg_match("/property=\"og:image\" content=\"([^\"]*)\"/", $body, $image);
        if($image)
        $product['image'] = @$image[1];
        preg_match_all("/data-zoom-image=\"([^\"]*)\"/", $body, $zoomimage);
        $product['zoomimage'] = @$zoomimage[1];
        preg_match_all("/data-image=\"([^\"]*)\"/", $body, $images);
        $product['images'] = @$images[1];
        preg_match_all("/data-image-initial=\"([^\"]*)\"/", $body, $imagesinitial);
        $product['imagesinitial'] = @$imagesinitial[1];
        preg_match_all("/data-swap-image=\"([^\"]*)\"/", $body, $swapimage);
        $product['swapimage'] = @$swapimage[1];
        $product['product_price'] = getStringByHieu($body, '<span id="product_price" class="hidden">', '</span>');
        $product['special_price_box'] = getStringByHieu($body, '<span id="special_price_box">', '</span>');
        $product['price_box'] = getStringByHieu($body, '<span id="price_box">', '</span>');
        $product['product_saving_percentage'] = getStringByHieu($body, '<span id="product_saving_percentage" class="price_highlight">', '</span>');
        $product['meta_product'] = json_decode(getStringByHieu($body, '<script type="application/ld+json">', '</script>'));
        $product['short_description'] = getStringByHieu($body, '<div class="prod_details">', '</div>');
        $product['specification_table'] = getStringByHieu($body, '<table class="specification-table">', '</table>');
        $product['product_infor'] = json_decode(getStringByHieu($body, 'var store = ', ';</script>'));
        $product['str_brecrum'] = str_replace('https://www.lazada.vn', base_url('danh-muc'), '<li class="breadcrumb__item'.getStringByHieu($body, '<li class="breadcrumb__item', '</ul>'));
        return $product;
    }
    public function getProduct($lug, $id)
    {
        $url = 'https://www.lazada.vn/'.$lug.'-'.$id.'.html';
        $body = curl_get($url, '', array(), 10, false);
//        echo $body;
//        die;
        return $this->genProduct($body);
    }
    public function getRecommend($sku = '', $venture = 'vn', $clientid = 'lzdMy', $apikey = '03e3b55b-cdb1-4a44-8f7e-79bfcc510a33')
    {
        $url = 'https://recommender.lazada.vn/recommendations?customerid=&sku='.$sku.'&venture='.$venture.'&clientid='.$clientid.'&apikey='.$apikey.'&placements=item_page.bottom&brand_id=&seller_id=&count=10';
        $body = curl_get($url, '', array(), 10, false);
        return $results = json_decode($body);
    }
    public function searchItem($keyword = '', $categoryids = '', $limit = 20, $newest = 0, $by = 'ctime', $order = 'desc')
    {
        $url = 'https://shopee.vn/api/v2/search_items/?by='.$by.'&match_id='.$categoryids.'&order='.$order.'&newest='.$newest.'&limit='.$limit.'&page_type=search';
        $body = curl_get($url, '', array("referer:https://shopee.vn/"));
        return json_decode($body);
//        $results = json_decode($body);
//        $url = 'https://shopee.vn/api/v1/items/';
//        $data = (object)array();
//        $data->item_shop_ids = $results->items;
//        $cookies = 'csrftoken=9hnTiNdxAMxkaxYmzrkXZ8lm0y7X4aBL;';
//        $body = $this->curl_post($url, json_encode($data), $cookies,
//            array('content-type:application/json',
//                'referer:https://shopee.vn/',
//                'x-csrftoken:9hnTiNdxAMxkaxYmzrkXZ8lm0y7X4aBL',
//                'x-requested-with:XMLHttpRequest'
//            ));
//        return json_decode($body);
    }
}
