<?php namespace App\Libraries;


use Config\App;
use Config\Services;
use App\Models\ProxyModel;
/**
 * Class General
 *
 * @package App\Libraries
 */
class Shopee
{
    public function __construct()
    {
        $this->proxy = new ProxyModel();
    }

    private function curl_get($url, $cookie = '', $options = array(), $timerequest = 25){
        // open a site with cookies
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_ENCODING, '');
        $browser = "Mozilla/5.0 (iPhone; CPU iPhone OS 10_3 like Mac OS X) AppleWebKit/602.1.50 (KHTML, like Gecko) CriOS/56.0.2924.75 Mobile/14E5239e Safari/602.1";
        if($cookie!=''){
            curl_setopt($ch, CURLOPT_COOKIE,str_replace(' ', '', $cookie));
        }
        curl_setopt($ch, CURLOPT_USERAGENT,$browser);
        if(isset($options['header']) && $options['header'])
        {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $options['header']);
        }
        if(isset($options['ip']) && $options['ip']!=0 && isset($options['port']) && $options['port']!=0){
            $proxy_ip = $options['ip'];
            $proxy_port = $options['port'];
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_PROXYPORT, $proxy_port);
            curl_setopt($ch, CURLOPT_PROXYTYPE, 'HTTPS');
            curl_setopt($ch, CURLOPT_PROXY, $proxy_ip);
            if(isset($options['username']) && isset($options['password']) && $options['username'] && $options['password']){
                $loginpassw = $options['username'].':'.$options['password'];
                curl_setopt($ch, CURLOPT_PROXYUSERPWD, $loginpassw);
            }
            curl_setopt($ch, CURLOPT_MAXREDIRS, 5);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
            curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, TRUE);
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);//đặt ở đây thì nó sẽ trả về kết quả mà không in ra ngay
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION  ,1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_TIMEOUT, $timerequest);

        $results = curl_exec($ch);
        curl_close($ch);
        return $results;
    }
    function curl_post($url,$data,$cookie='',$header = array(),$host=0,$port=0,$browser="Mozilla/5.0 (Windows NT 6.1; rv:11.0) Gecko/20100101 Firefox/11.0"){
        // open a site with cookies
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_VERBOSE, 0);
        if($cookie!='') curl_setopt($ch, CURLOPT_COOKIE,$cookie);
        curl_setopt($ch, CURLOPT_USERAGENT,$browser);
        curl_setopt($ch, CURLOPT_HEADER  ,0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER  ,TRUE);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION  ,TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        if($host!=0 && $port!=0){
            // echo "proxy";
            $proxy=$host.":".$port;
            curl_setopt($ch, CURLOPT_PROXY, $proxy);
            curl_setopt($ch, CURLOPT_MAXREDIRS, 5);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
            curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, TRUE);
        }
        if($header)
        {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        }
        curl_setopt($ch, CURLOPT_POST,1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$data);
        return curl_exec($ch);
    }
    public function getCats()
    {
        $url = 'https://shopee.vn/api/v1/category_list/';
        $results = $this->curl_get($url);
        $catlist = json_decode($results);
        return $catlist;
    }
    public function getProduct($shopid, $itemid)
    {
        $url = 'https://shopee.vn/api/v2/item/get?itemid='.$itemid.'&shopid='.$shopid;
        $body = $this->curl_get($url);
        return json_decode($body);
    }
    //USING PROXY

    public function getRecommend($catid = '-2', $recommend_type = 6, $limit = 60, $set = 0)
    {
        $url = 'https://shopee.vn/api/v2/recommend_items/get?catid='.$catid.'&recommend_type='.$recommend_type.'&limit='.$limit.'&offset='.$set.'&by=price&order=desc';
        $header = array("referer:https://shopee.vn/");
        $body = $this->proxy_to_result_get($url, $header);
        return $body;
    }
    public function searchItem($keyword = '', $categoryids = '', $limit = 20, $newest = 0, $by = 'ctime', $order = 'desc')
    {
        $headers =  array(":authority: shopee.vn",
            "referer:https://shopee.vn/",
            'if-none-match: "596e3cab3abe804f85e3d86b8e16cd43;gzip"',
            "if-none-match-: 55b03-11c0052ae7a3a194e6b023f27b536fbf",
            "content-type:application/json"
        );
        $key = !empty($keyword) ? '&keyword='.urlencode($keyword) : '';
        $url = 'https://shopee.vn/api/v2/search_items/?by='.$by.$key.'&match_id='.$categoryids.'&order='.$order.'&newest='.$newest.'&limit='.$limit.'&page_type=search';
        $body = $this->proxy_to_result_get($url, $headers);
        return $body;
    }
    public function searchKeyword($params = []){
        if($params){
            $by = "sales";
            $order = "desc";
            $keyword = "balo%20da";
            $limit = 50;
            $offset = 0;
            $page_type = "search";
            if(isset($params['by'])){
                $by = $params['by'];
            }
            if(isset($params['order'])){
                $order = $params['order'];
            }
            if(isset($params['limit'])){
                $limit = $params['limit'];
            }
            if(isset($params['offset'])){
                $offset = $params['offset'];
            }
            if(isset($params['page_type'])){
                $page_type = $params['page_type'];
            }
            if(isset($params['keyword'])){
                $keyword = urlencode($params['keyword']);
            }
            $url = "https://shopee.vn/api/v2/search_items/?by=$by&keyword=$keyword&limit=$limit&newest=$offset&order=$order&page_type=$page_type";
            $header = array("referer:https://shopee.vn/", "content-type:application/json");
            $body = $this->proxy_to_result_get($url, $header);
            return $body;
        }
        return false;
    }
    public function shop_detail($id){
        // https://shopee.vn/api/v2/shop/get?is_brief=1&shopid=30728253
        $url = 'https://shopee.vn/api/v2/shop/get?is_brief=1&shopid='.$id;
        $header = array("referer:https://shopee.vn/");
        $body = $this->proxy_to_result_get($url, $header);
        return $body;
    }


    public function shop_product($params=[]){
        // https://shopee.vn/api/v2/search_items/?by=pop&limit=30&match_id=71861331&newest=60&order=desc&page_type=shop&version=2
        // by: pop
        // limit: 30
        // match_id: 71861331
        // newest: 60
        // order: desc
        // page_type: shop
        // version: 2
        if($params){
            $by = "pop";
            $order = "desc";
            $match_id = 71861331;
            $limit = 50;
            $offset = 0;
            $page_type = "shop";
            $version = 2;
            if(isset($params['by'])){
                $by = $params['by'];
            }
            if(isset($params['order'])){
                $order = $params['order'];
            }
            if(isset($params['limit'])){
                $limit = $params['limit'];
            }
            if(isset($params['offset'])){
                $offset = $params['offset'];
            }
            if(isset($params['page_type'])){
                $page_type = $params['page_type'];
            }
            if(isset($params['match_id'])){
                $match_id = urlencode($params['match_id']);
            }
            $url = "https://shopee.vn/api/v2/search_items/?by=$by&limit=$limit&match_id=$match_id&newest=$offset&order=$order&page_type=$page_type&version=$version";
            $header = array("referer:https://shopee.vn/", "content-type:application/json");
            $body = $this->proxy_to_result_get($url, $header);
            return $body;
        }
        return false;
    }

    public function proxy_to_result_get($url, $header ='', $cookie = ''){
        $proxy = $this->proxy->getProxy();
        $options = $proxy;
        $options['header'] = $header;
        $body = $this->curl_get($url, $cookie, $options);
        if($proxy){
            $this->proxy->updateProxy(['id' => $proxy['id']], ['date_using' => date('Y-m-d H:i:s')]);
        }
        return json_decode($body);
    }
    public function checkProxy(){
        $list_proxy = array('23.89.29.6',
        '23.89.29.8',
        '23.89.29.43',
        '23.89.29.44',
        '23.89.29.45',
        '23.89.29.46',
        '23.89.29.47',
        '23.89.29.48',
        '23.89.29.49',
        '23.89.29.50',
        '23.89.29.51',
        '23.89.29.52',
        '23.89.29.66',
        '23.89.29.67',
        '23.89.29.68',
        '23.89.29.69',
        '23.89.29.79',
        '23.89.29.80',
        '23.89.29.81',
        '23.89.29.82',
        '23.88.143.159',
        '23.88.143.160',
        '23.89.143.173',
        '23.89.143.174',
        '23.89.143.175',
        '23.89.144.88',
        '23.89.144.218',
        '23.89.144.219',
        '23.89.175.2',
        '23.89.175.8',
        '23.89.175.50',
        '23.89.175.82',
        '23.89.175.100',
        '23.89.175.108',
        '23.89.175.109',
        '23.89.175.110',
        '23.89.175.116',
        '23.89.175.131',
        '23.89.175.214',
        '23.89.175.215',
        '23.89.175.222',
        '23.89.175.223',
        '23.89.175.224',
        '23.89.175.225',
        '23.89.175.230',
        '23.89.175.238',
        '23.89.175.248',
        '23.89.175.249',
        '23.89.175.250',
        '23.89.175.251',
        '23.89.175.252',
        '23.89.176.47',
        '23.89.176.48',
        '23.89.176.56',
        '23.89.176.57',
        '23.89.176.220',
        '23.89.176.221',
        '23.89.176.222',
        '23.89.176.223',
        '23.89.176.225',
        '23.89.176.251',
        '23.89.176.252',
        '23.89.176.253',
        '23.89.176.254',
        '23.88.160.74',
        '23.88.160.75',
        'p1.vietpn.co',
        'p2.vietpn.co',
        'p4.vietpn.co',
        'p10.vietpn.co',
        'p14.vietpn.co',
        'p15.vietpn.co',
        's4.vietpn.co',
        '123.31.45.40',
        'p20.vietpn.co',
        's9.vietpn.co',
        '42.112.30.20',
        '42.112.30.23',
        '42.112.30.30',
        'v1.vietpn.co',
        'v2.vietpn.co',
        'v3.vietpn.co',
        'v4.vietpn.co',
        'v5.vietpn.co',
        'v6.vietpn.co',
        'v7.vietpn.co',
        'v8.vietpn.co',
        'v9.vietpn.co',
        'v10.vietpn.co',
        'v11.vietpn.co',
        'v12.vietpn.co',
        'v13.vietpn.co',
        'v14.vietpn.co',
        'v15.vietpn.co',
        'v16.vietpn.co',
        'v17.vietpn.co',
        'v18.vietpn.co',
        'v19.vietpn.co',
        'v20.vietpn.co',
        'v21.vietpn.co',
        'v22.vietpn.co',
        'v23.vietpn.co',
        'v24.vietpn.co',
        'v25.vietpn.co',
        'v26.vietpn.co',
        'v27.vietpn.co',
        'v28.vietpn.co',
        'v29.vietpn.co',
        'v30.vietpn.co',
        'v31.vietpn.co',
        'v32.vietpn.co',
        'v33.vietpn.co',
        'p16.vietpn.co',
        'p22.vietpn.co',
        'p23.vietpn.co',
        'p3.vietpn.co',
        'p21.vietpn.co',
        'p6.vietpn.co',
        'v34.vietpn.co',
        'v35.vietpn.co',
        'v36.vietpn.co',
        'v39.vietpn.co',
        'v40.vietpn.co',
        'v41.vietpn.co',
        'v41.vietpn.co',
        '125.212.251.75',
        'flycaching.com',
        '172.245.249.126',
        '149.28.133.243',
        's5.vietpn.co');
    foreach($list_proxy as $ip){
        if($this->proxy->checkProxy($ip)){
            echo $ip.'</br>';
        } else {
            echo $ip.'----</br>';
        }
    }
    die;
    }
}