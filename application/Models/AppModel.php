<?php namespace App\Models;

use CodeIgniter\Model;
use Config\Database;

/**
 * Class CatModel
 *
 * @package App\Models
 */
class AppModel extends Model
{

    /**
     * @var \CodeIgniter\Database\BaseBuilder
     */
    private $_table;

    /**
     * Site constructor.
     *
     * @param array ...$params
     *
     * @throws \CodeIgniter\Database\Exceptions\DatabaseException
     */
    public function __construct(...$params)
    {
        parent::__construct(...$params);
    }
}
