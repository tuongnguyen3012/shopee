<?php namespace App\Models;

use CodeIgniter\Model;
use Config\Database;

/**
 * Class CatModel
 *
 * @package App\Models
 */
class CatModel extends AppModel
{

    /**
     * @var \CodeIgniter\Database\BaseBuilder
     */
    private $cat_table;
    /**
     * Site constructor.
     *
     * @param array ...$params
     *
     * @throws \CodeIgniter\Database\Exceptions\DatabaseException
     */
    protected $table = 'categories';
    protected $allowedFields = ['catid', 'parent_category', 'sort_weight', 'is_adult', 'name', 'display_name', 'slug', 'descriptions', 'tags', 'image', 'status', 'created_at', 'in_menu', 'results'];
    //for sexyfr
    // protected $table = 'category';
    // protected $allowedFields = ['id', 'name', 'slug', 'collectionid', 'description', 'content', 'image', 'parent_id', 'in_menu', 'type', 'results', 'created_at'];
    public function __construct(...$params)
    {
        parent::__construct(...$params);
        $this->db = Database::connect();
        $this->cat_table = $this->db->table($this->table);
    }
    public function getByIdShopee($catid_shopee)
    {
        $this->cat_table->select();
        $this->cat_table->where('collectionid', $catid_shopee);
        return $this->cat_table->get()->getResult('array');
    }
    /**
     * @return array
     */
    public function getlist($params = []):array
    {
        $this->cat_table->select();
        if($params && isset($params['not_check_parent'])){
        } else {
            $this->cat_table->where('parent_category', '0');
        }
        if($params && isset($params['start']) && isset($params['limit'])){
            $this->cat_table->limit($params['limit'], $params['start']);
        }
        $this->cat_table->orderBy('id', 'ASC');
        return $this->cat_table->get()->getResult('array');
    }
    public function getMenu($params = []):array
    {
        $this->cat_table->select();
        $this->cat_table->where('in_menu', '1');
        $this->cat_table->limit(25, 0);
        $this->cat_table->orderBy('id', 'ASC');
        return $this->cat_table->get()->getResult('array');
    }
    /**
     * @param int $id
     * @param string $column
     * @param string $data
     *
     * @return bool
     */
    public function UpdateCat(int $id, string $column, string $data):bool
    {
        $this->cat_table->set($column, $data);
        $this->cat_table->where('id', $id);
        $this->cat_table->update();

        return true;
    }
    public function updateCategory($where = [], $data){
        if($where){
            $this->cat_table->where($where);
        }
        $this->cat_table->update($data);
    }

    /**
     * @param string $title
     * @param string $content
     * @param string $slug
     * @param string $icon
     */
    public function AddCat($data)
    {
        $this->cat_table->insert($data);
    }
}
