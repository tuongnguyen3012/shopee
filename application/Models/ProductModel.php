<?php namespace App\Models;

use CodeIgniter\Model;
use Config\Database;

/**
 * Class CatModel
 *
 * @package App\Models
 */
class ProductModel extends AppModel
{
    public $product_table;
    protected $table = 'products';
    public function __construct(...$params)
    {
        parent::__construct(...$params);
        $this->db = Database::connect();
        $this->product_table = $this->db->table('products');
    }
    protected $key_allows = array(
        'itemid',
        'shopid',
        'catid',
        'sub_catid',
        'third_catid',
        'name',
        'image',
        'rating_count',
        'rating_star',
        'description',
        'price',
        'discount',
        'show_discount',
        'price_before_discount',
    );
    public function getByIdShopee($productid_shopee, $shopid_shopee)
    {
        $this->product_table->select();
        $this->product_table->where('itemid', $productid_shopee);
        $this->product_table->where('shopid', $shopid_shopee);
        $query = $this->product_table->get();
        $result = $query->getRowArray();
        $query->freeResult();
        return $result;
    }
    public function addproduct($datas)
    {
        $product = array();
        if(is_object($datas))
        {
            $datas = ((array)$datas);
        }
        foreach($this->key_allows as $key)
        {
            if(isset($datas[$key]))
            {
                if(is_array($datas[$key]) || is_object($datas[$key]))
                {
                    $product[$key] = json_encode($datas[$key]);
                }
                else
                {
                    $product[$key] = $datas[$key];
                }
            }
        }
        $product['status'] = 1;
        $product['created_at'] = date('Y-m-d H:i:s');
        $product['updated_at'] = date('Y-m-d H:i:s');
        $product['data_json'] = json_encode($datas);
       // if($this->product_table->getByIdShopee)
        $this->product_table->insert($product);
    }
}
