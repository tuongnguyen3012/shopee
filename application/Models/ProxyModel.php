<?php namespace App\Models;

use CodeIgniter\Model;
use Config\Database;

/**
 * Class CatModel
 *
 * @package App\Models
 */
class ProxyModel extends AppModel
{
    public $proxy_table;
    protected $table = 'proxies';
    public function __construct(...$params)
    {
        parent::__construct(...$params);
        $this->db = Database::connect();
        $this->proxy_table = $this->db->table($this->table);
    }
    protected $key_allows = array(
        'ip', 'port', 'username', 'password', 'local', 'date_using', 'status'
    );
    public function getProxy()
    {
        $this->proxy_table->select('id,ip,port,username,password');
        $this->proxy_table->where('status', 1);
        $this->proxy_table->orderBy('date_using', 'ASC');
        $query = $this->proxy_table->get();
        $result = $query->getRowArray();
        $query->freeResult();
        return $result;
    }

    public function checkProxy($ip)
    {
        $this->proxy_table->select('id,ip,port,username,password');
        $this->proxy_table->where('ip', $ip);
        $query = $this->proxy_table->get();
        $result = $query->getRowArray();
        $query->freeResult();
        return $result;
    }

    public function updateProxy($where = [], $data){
        if($where){
            $this->proxy_table->where($where);
        }
        $this->proxy_table->update($data);
    }
}
