<div class="row">
    <div class="col-md-12"> 
        <nav aria-label="Page navigation">
            <ul class="pagination">
            <?php  $pager = $results['pager'];  echo $pager->links(); ?>
            </ul>
        </nav>
    </div>
    <div class="col-md-12">
        <table class="table">
            <tr>
                <th>ID</th>
                <th>Cat ID</th>
                <th>Name</th>
                <th>Is Menu</th>
                <th>Action</th>
            </tr>
            <?php foreach($results['data'] as $value): ?>
            <tr>
                <td><?=$value['id']?></td>
                <td><?=$value['catid']?></td>
                <td><?=$value['display_name']?></td>
                <td><?=$value['in_menu']?></td>
                <td><a href="<?=base_url('/admin/category/edit/'.$value['id'])?>">Edit</a></td>
            </tr>
            <?php endforeach; ?>
        </table>
    </div>
</div>