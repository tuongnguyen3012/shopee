<div class="row">
    <h3>Cấu hình site</h3>
<form method="post">
    <div class="col-md-4">
        <div class="form-group">
            <label>Prefix product</label>
            <input class="form-control" name="config[prefix_product]" value="<?=isset($configs->prefix_product) ? $configs->prefix_product : ""?>">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label>Prefix category</label>
            <input class="form-control" name="config[prefix_category]" value="<?=isset($configs->prefix_category) ? $configs->prefix_category : ""?>">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label>Prefix search</label>
            <input class="form-control" name="config[prefix_search]" value="<?=isset($configs->prefix_search) ? $configs->prefix_search : ""?>">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label>Prefix shop</label>
            <input class="form-control" name="config[prefix_shop]" value="<?=isset($configs->prefix_shop) ? $configs->prefix_shop : ""?>">
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="form-group">
        <label>Meta title</label>
        <input class="form-control" name="config[meta_title]" value="<?=isset($configs->meta_title) ? $configs->meta_title : ""?>"/>
    </div>
    <div class="form-group">
        <label>Meta description</label>
        <textarea class="form-control" name="config[meta_description]"><?=isset($configs->meta_description) ? $configs->meta_description : ""?></textarea>
    </div>
    <div class="form-group">
        <label>Header</label>
        <textarea class="form-control" name="config[header]"><?=isset($configs->header) ? $configs->header : ""?></textarea>
    </div>
    <div class="form-group">
        <label>Footer</label>
        <textarea class="form-control" name="config[footer]"><?=isset($configs->footer) ? $configs->footer : ""?></textarea>
    </div>
    <button class="btn btn-primary">SUBMIT</button>
</form>
</div>