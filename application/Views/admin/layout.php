<html lang="vi">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>Quản trị</title>

    <link rel="stylesheet" href="<?php echo base_url('assets/css/front.css'); ?>">
    <script src="<?php echo base_url('assets/js/jquery.min.js'); ?>"></script>
</head>
<body>
    <div class="container">
        <?php echo $content; ?>
    </div>
</body>
</html>