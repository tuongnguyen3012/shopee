<div class="row">
    <h3>Cấu hình menu</h3>
<form method="post">
    <div class="form-group">
        <label>Menu Hot Key</label>
        <textarea class="form-control" name="config[hotkey]"><?=isset($configs->hotkeys) ? implode("\n", $configs->hotkeys) : ""?></textarea>
    </div>
    <div class="form-group">
        <label>Menu HOME</label>
        <textarea class="form-control" name="config[homemenu]"><?=isset($configs->homemenu) ? implode("\n", $configs->homemenu) : ""?></textarea>
    </div>
    <button class="btn btn-primary">SUBMIT</button>
</form>
</div>