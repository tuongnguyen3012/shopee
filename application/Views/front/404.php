<div class="text-center" style="padding-top: 30px;padding-bottom: 30px;">
    <a href="<?php echo base_url('/'); ?>" class="text-logo" title="<?=META_TITLE?>">
        <img src="<?=env('SITE_LOGO')?>">
    </a><br/>
    <img src="/assets/img/404.png">
    <div>Xin lỗi, sản phẩm này không tồn tại! Trở lại <a href="/">trang chủ</a></div>
</div>
