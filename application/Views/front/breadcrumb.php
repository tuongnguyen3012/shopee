<div id="breadcrumb">
    <div class="container">
        <ol class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
            <li class="breadcrumb__item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                 <span class="breadcrumb__item-text">
                     <a itemprop="item" href="/" title="Trang chủ"><span itemprop="name">Trang chủ</span></a>
                     <meta itemprop="position" content="1" />
                 </span>
            </li>
            <?php foreach($breadcrumbs as $key => $breadcrumb){ ?>
                <li class="breadcrumb__item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                <span class="breadcrumb__item-text">
                    <a title="<?= $breadcrumb['name']; ?>" href="<?= $breadcrumb['url']; ?>" class="breadcrumb__item-anchor" itemprop="item">
                        <span itemprop="name"><?= $breadcrumb['name']; ?></span>
                    </a>
                    <meta itemprop="position" content="<?php echo $key+2; ?>">
                </span>
            </li>
            <?php } ?>
        </ol>
    </div>
</div>