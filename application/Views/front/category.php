<div id="home_main" class="page-wrapper">
    <div class="container">
        <div id="box_phone_lastest" class="home-main-block row">
            <div class="col-xs-12">
                <h1 class="bn-header"><strong><?= $cate_name ? $cate_name : $category['display_name']?></strong>
                </h1>
                <?php if(isset($sub_category) && $sub_category) : ?>
                <div class="row list-sub-cat">
                    <?php foreach($sub_category as $sub_cat):?>
                    <div class="item-sub-cat col-md-1 col-xs-3 text-center">
                        <a href="<?=cat_url($sub_cat['display_name'], $sub_cat['catid'])?>">
                            <img src="<?=image_shopee($sub_cat['image'])?>" alt="<?=$sub_cat['display_name']?>"/>
                            <small><?=$sub_cat['display_name']?></small>
                        </a>
                    </div>
                    <?php endforeach; ?>
                </div>
                <?php endif; ?>
                <div class="row">
                    <div class="col-md-12">
                        <?php if(isset($short_description) && $short_description): ?>
                            <p><?=$short_description?></p>
                        <?php endif; ?>
                        <div class="pull-left">
                            <h2 class="title-group">Gợi ý cho bạn những sản phẩm "<?=$cate_name?>" dưới đây:</h2>
                        </div>
                        <?php 
                            if(isset($sort)): 
                                $url_sort = isset($url_search) ? $url_search.'&' : current_url().'?';
                            else:
                                $url_sort = current_url().'?';
                            endif;
                        ?>
                        <div class="sortProduct dropdown pull-right">
                            <button class="btn dropdown-toggle" type="button" data-toggle="dropdown"><?=isset($sort) && !empty(sortConvertText($sort)) ? sortConvertText($sort)['text']: "Bán Chạy"?>&nbsp;<span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li class="<?= (isset($sort) && $sort== 'sales_desc') ? 'active':''?>"><a href="<?=$url_sort.'sort=sales_desc'?>"><?=!empty(sortConvertText('sales_desc')) ? sortConvertText('sales_desc')['text']: ""?></a></li>
                                <li class="<?= (isset($sort) && $sort== 'ctime_desc') ? 'active':''?>"><a href="<?=$url_sort.'sort=ctime_desc'?>"><?=!empty(sortConvertText('ctime_desc')) ? sortConvertText('ctime_desc')['text']: ""?></a></li>
                                <li class="<?= (isset($sort) && $sort== 'pop_desc') ? 'active':''?>"><a href="<?=$url_sort.'sort=pop_desc'?>"><?=!empty(sortConvertText('pop_desc')) ? sortConvertText('pop_desc')['text']: ""?></a></li>
                                <li class="<?= (isset($sort) && $sort== 'price_asc') ? 'active':''?>"><a href="<?=$url_sort.'sort=price_asc'?>"><?=!empty(sortConvertText('price_asc')) ? sortConvertText('price_asc')['text']: ""?></a></li>
                                <li class="<?= (isset($sort) && $sort== 'price_desc') ? 'active':''?>"><a href="<?=$url_sort.'sort=price_desc'?>"><?=!empty(sortConvertText('price_desc')) ? sortConvertText('price_desc')['text']: ""?></a></li>
                            </ul>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <?php $list_name = '';?>
                <?php if(isset($items) && $items) { ?>
                    <div class="row">
                        <?php foreach($items as $key => $item){ ?>
                            <?php
                                $prefix = ($key > 0) ? '<span class="text-danger">; </span>' : "";
                                $list_name .= $prefix.$item->name; 
                            ?>
                            <?php echo view('front/itemlist', array('item' => $item)) ?>
                        <?php } ?>
                    </div>
                    <div class="col-md-12 text-center">
                        <ul class="pagination">
                            <li><a href="<?=isset($prev_link) ? $prev_link : ''?>">Trang trước</a></li>
                            <li class="active"><a href="<?=isset($current_link) ? $current_link : ''?>">Trang hiện tại</a></li>
                            <?php if(count($items)>=$limit):?><li><a href="<?=isset($next_link) ? $next_link : ''?>">Trang sau</a></li><?php endif; ?>
                        </ul>
                    </div>
                <?php } else { echo '<div>Nếu không tìm thấy sản phẩm nào, vui lòng <a href="">tải lại trang</a></div>';} ?>
                <div>
                    <h2 class="title-group">Những sản phẩm "<?=$cate_name?>" được tìm thấy:</h2>
                    <?php if(isset($page_image) && $page_image): ?><img src="<?=$page_image?>" alt="<?=$page_title?>"/><?php endif;?>
                    <p>
                        <?=$list_name?>
                    </p>
                    <?php if(!empty($key_suggest)): ?>
                        <?php foreach($key_suggest as $q): ?>
                            <a href="<?=search_url($q)?>"><?=$q?></a>; 
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>