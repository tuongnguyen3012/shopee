<div id="home_main" class="page-wrapper">
    <div class="container">
        <div id="box_phone_lastest" class="home-main-block">
            <div class="col-xs-12">
                <h1 class="bn-header">
                    <?php if(isset($menu_hot_keys['homemenu'][0]) && $menu_hot_keys['homemenu'][0]): ?>
                        <a href="<?= search_url($menu_hot_keys['homemenu'][0]) ?>" title="<?=$menu_hot_keys['homemenu'][0]?> giá cực tốt"><?=$menu_hot_keys['homemenu'][0]?> giá cực tốt</a>
                    <?php else: ?>
                        <strong>Khuyến mãi hot</strong><span class="hidden-xs"> trong ngày </span>
                    <?php endif; ?>
                </h1>
                <div class="row">
                    <?php if(isset($items) && $items) { ?>
                        <?php foreach($items as $item){ ?>
                            <?php echo view('front/itemlist', array('item' => $item)) ?>
                        <?php } ?>
                    <?php } ?>
                </div>
            </div>
        </div>
        <div class="home-list-category">
            <?php foreach($categries as $cat){ ?>
                <a href="<?php echo cat_url($cat['display_name'], $cat['catid']); ?>" title="<?php echo $cat['display_name']; ?>"><?php echo $cat['display_name']; ?></a> |
            <?php } ?>
        </div>
    </div>
</div>