<div class="item col-xs-6 col-sm-3 col-md-2 box-category-item">
    <div class="item-box">
        <a href="<?= product_url($item->shopid, $item->itemid, $item->name) ?>" title="<?= $item->name; ?>">
            <?php if(strlen($item->image) < 32 && empty($item->price)): ?>
                <img data-original="<?=file_exists(image_shopee($item->image, 'tn')) ? image_shopee($item->image, 'tn') : image_shopee($item->image, 'tn')?>"
                     class="lazy" src="<?=base_url('assets/img/default-image-select-buy.jpg')?>"
                     style="display: block;">
                <span class="item-title"><?= name_shopee($item->name) ?></span>
                <?php if(isset($item->discount)){ ?>
                    <span class="promotion">&nbsp;(Giảm - <?= discount_shopee($item->discount); ?>)</span>
                <?php } ?>
                <div class="btn btn-detail">Xem chi tiết</div>
            <?php else: ?>
                <img data-original="<?php echo image_shopee($item->image, 'tn'); ?>"
                     class="lazy" src="<?=base_url('assets/img/default-image-select-buy.jpg')?>"
                     style="display: block;">
                <span class="item-title"><?php echo name_shopee($item->name); ?></span>
                <span class="item-price"><?php echo price_shopee($item->price); ?></span>
                <?php if(isset($item->price_before_discount) && $item->price_before_discount): ?>
                    <span class="price-old"><?= price_shopee($item->price_before_discount); ?></span>
                    <div class="promotion">(giảm <?php echo $item->discount?>)</div>
                <?php endif; ?>
            <?php endif; ?>
        </a>
    </div>
</div>