<html lang="vi">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title><?= my_ucwords($page_title) . ' - ' . $_SERVER['HTTP_HOST'] ?></title>
    <meta name="description" content="<?php $page_description = substr($page_description, 0, 320); echo $page_description; ?>">
    <link rel="canonical" href="<?= current_url(); ?>">
    <link rel="shortcut icon" href="<?php echo base_url('assets/img/favicon.png'); ?>" type="image/x-icon"/>
    <meta property="fb:app_id" content="" />
    <meta property="og:type"               content="product" />
    <meta property="og:url"                content="<?= current_url(); ?>" />
    <meta property="og:title"              content="<?= my_ucwords($page_title) . ' - ' . $_SERVER['HTTP_HOST']?>" />
    <meta property="og:description"        content="<?= $page_description; ?>" />
    <meta property="og:image"              content="<?= $page_image; ?>" />

    <link rel="stylesheet" href="<?php echo base_url('assets/css/front.css'); ?>">
    <script src="<?php echo base_url('assets/js/jquery.min.js'); ?>"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
    <?=CONFIG_HEADER?>
</head>
<body>
    <header id="header">
        <div class="header-top">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-3 col-md-3">
                        <button type="button" class="visible-xs bnam-menu is-closed" data-toggle="offcanvas"><span
                                class="hamb-top"></span><span class="hamb-middle"></span><span class="hamb-bottom"></span>
                        </button>
                        <a href="<?php echo base_url('/'); ?>" class="text-logo navbar-brand"
                           title="<?=META_TITLE?>"><img
                                src="<?=env('SITE_LOGO')?>"></a></div>
                    <div class="hidden-xs col-sm-7 col-md-6">
                        <form method="GET" action="<?=site_url(PREFIX_SEARCH)?>" class="search-form">
                            <div class="input-group form">
                                <input id="search-top" name="q" class="form-control"
                                       placeholder="Tìm kiếm sản phẩm..." value="" type="text">
                                <span class="input-group-btn">
                                    <button class="btn btn-danger" type="submit">Tìm kiếm</button>
                                </span>
                            </div>
                        </form>
                        <?php $hot_keys = isset($menu_hot_keys['hotkeys']) && $menu_hot_keys['hotkeys'] ? $menu_hot_keys['hotkeys'] : []; ?>
                        <div class="hot-keys">
                            <?php $list_hot_key = ''; if($hot_keys): ?>
                                <?php foreach ($hot_keys as $hot_key ): ?>
                                    <?php $list_hot_key .= '<a href="'.search_url($hot_key).'" title="'.$hot_key.'">'.$hot_key.'</a>';?>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="col-md-3 text-right">
                        <div class="fb-like" data-href="<?=current_url()?>" data-width="" data-layout="button" data-action="like" data-size="small" data-share="true"></div>
                    </div>
                </div>
            </div>
        </div>
        <div id="search-mobi" class="container visible-xs">
            <div class="row">
                <div class="col-xs-12">
                    <form method="GET" action="<?=site_url(PREFIX_SEARCH)?>" class="search-form">
                        <div class="input-group form">
                            <input name="q" class="search-top form-control" placeholder="Tìm kiếm sản phẩm..." value="" type="text">
                            <span class="input-group-btn">
                                <button class="btn btn-danger btn-search-top" type="submit">Tìm kiếm</button>
                            </span>
                        </div>
                    </form>
                    <div class="hot-keys"><?=$list_hot_key?></div>
                </div>
            </div>
        </div>
        <div id="header-nav">
            <div class="container">
                <nav role="navigation" id="op-navbar" class="row">

                    <ul role="menubar" class="nav navbar-nav">
                        <?php echo $menu; ?>
                    </ul>
                </nav>
            </div>
        </div>
    </header>
    <?php if (isset($breadcrumbs)) { ?>
        <?php echo view('front/breadcrumb', array('breadcrumbs' => $breadcrumbs)); ?>
    <?php } ?>
    <main>
        <?php echo $content; ?>
    </main>
    
    <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/lazyload.js'); ?>"></script>
    <footer>
    <?php if(CONFIG_FOOTER) : ?>
        <?=CONFIG_FOOTER?>
    <?php else: ?>
        <div class="container">
            <p><strong>Mua Hàng Siêu Rẻ</strong></p>
            <p>Thiên Đường Mua Sắm Trực Tuyến shopee Với Hàng Ngàn Sản Phẩm Ưu Đãi Hấp Dẫn, Giao Hàng Nhanh Chóng, Chất
                Lượng Tin Cậy, Hài Lòng Khách Hàng, Đảo Bảo Giá Ưu Đãi Brands: Philips, Infinix, New Look, Xiaomi,
                Huggies.<br>Thỏa thích mua sắm hàng triệu sản phẩm từ hàng nghìn shop, 100% đảm bảo với shopee! Ứng dụng mua
                sắm đa năng, Tổng đài hỗ trợ 24/7, Vận chuyển toàn quốc, Thanh toán đảm bảo. </p>
        </div>
        <div class="container">
            <p class="text-center">2019 © Khuyến mãi shopee, mã giảm giá, mua hàng shopee
                giá rẻ</p>
        </div>
    <?php endif; ?>
    </footer>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.bnam-menu, .overlay').click(function () {
                if ($('#header-nav').hasClass('bmenu')) {
                    $('#header-nav').removeClass('bmenu');
                    $('.overlay').hide();
                    $('.bnam-menu').removeClass('is-open').addClass('is-closed');
                } else {
                    $('#header-nav').addClass('bmenu');
                    $('.overlay').show();
                    $('.bnam-menu').removeClass('is-closed').addClass('is-open');
                }
            });
            $(window).bind('scroll', function () {
                if ($(window).scrollTop() > 50) {
                    $('#header-nav').addClass('fixed');
                } else {
                    $('#header-nav').removeClass('fixed');
                }
            });
            if ($("img.lazy").length > 0) {
                $("img.lazy").lazyload({effect: "fadeIn", effectspeed: 300});
            }
        });
    </script>
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v3.0&appId=1327839193893062&autoLogAppEvents=1';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
</body>
</html>