<link href="<?= base_url('/assets/css/lightslider.min.css'); ?>" rel="stylesheet">
<div id="product_main" class="page-wrapper">
    <div class="container">
        <section>
            <div class="product-detail">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-5">
                        <?php if($product->images): ?>
                            <?php if(count(json_decode(json_encode($product->images), true)) >  1): ?>
                                <ul id="imageGallery" class="gallery list-unstyled lightSlider lSSlide" style="width: 1468px; transform: translate3d(-367px, 0px, 0px); height: 367px; padding-bottom: 0%;">
                                    <?php foreach($product->images as $key => $image){ ?>
                                        <li data-thumb="<?php echo image_shopee($image); ?>">
                                            <img src="<?php echo image_shopee($image); ?>" alt = "<?=$product->name . ' - ' . $key?>">
                                        </li>
                                    <?php } ?>
                                </ul>
                            <?php else: ?>
                                <?php foreach($product->images as $key => $image){ ?>
                                    <img src="<?php echo image_shopee($image); ?>" alt = "<?=$product->name . ' - ' . $key?>">
                                <?php } ?>
                            <?php endif; ?>
                        <?php endif; ?>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-7" id="product-short">
                        <div class="product-infor">
                            <div class="product-head">
                                <h1 class="page-title">
                                    <?= name_shopee($product->name) ?>
                                    <?php if($product->price){ ?> Giá chỉ <?php echo price_shopee($product->price); ?><?php } ?>
                                </h1>
                            </div>
                            <div class="review-pro">
                                <?php if(isset($product->historical_sold) && $product->historical_sold):?>&#10003; Đã bán : <span><?=number_format($product->historical_sold, 0, '.', '.')?></span><?php endif; ?>
                                <?php if(isset($product->item_rating) && $product->item_rating): ?>
                                    &#9997;Đánh giá: <span><?= isset($product->item_rating->rating_count[0])? number_format($product->item_rating->rating_count[0], 0, '.', '.'): 0 ?></span>    
                                    &#10024;<span><?= isset($product->item_rating->rating_star)? round($product->item_rating->rating_star, 1): 0 ?></span>
                                <?php endif; ?>
                            </div>
                            <?php $shopmall = false;if(isset($shop_detail) && $shop_detail && $shop_detail->show_official_shop_label){$shopmall = true;} ?>
                            <div class="product-price-wrap <?=$shopmall?'price-shopmall':''?>">
                                <?php if($product->price){ ?>
                                    <span class="product-price"><?= price_shopee($product->price); ?></span>
                                    <?php if(isset($product->price_before_discount) && $product->price_before_discount): ?>
                                        <span class="price-old"><?= price_shopee($product->price_before_discount); ?></span>
                                        <span class="percent-discount">(giảm <?php echo $product->discount?>)</span>
                                    <?php endif; ?>
                                <?php } ?>
                            </div>
                            <p style="color: #ff5722;">
                                <img width="28" src="https://cdngarenanow-a.akamaihd.net/shopee/shopee-pcmall-live-sg/assets/d7cb3c110ef860cca3969ab0cd6c2ac9.png">
                                Ở đâu rẻ hơn, Shopee hoàn tiền
                            </p>
                            <?php if(isset($product->coin_info->coin_earn_items[0]->coin_earn)):?>
                                <p><img style="vertical-align: sub;" src="https://cdngarenanow-a.akamaihd.net/shopee/shopee-pcmall-live-sg/assets/a507d0e185dbd56e388652d8d8da845d.png" width="16" height="16"> 
                                    Mua hàng và tích <span class="text-danger"><?=$product->coin_info->coin_earn_items[0]->coin_earn?></span> xu, sử dụng cho những lần mua hàng tiếp theo (1000 xu = 1000 đ)
                                </p>
                            <?php endif; ?>
                        </div>
                        <div class="tier_variations">
                            <?php 
                                if(isset($product->tier_variations) && $product->tier_variations): 
                                foreach($product->tier_variations as $variation):
                            ?>
                                <div class="item-variation">
                                    <div class="name-variation"><?=isset($variation->name) ? $variation->name : ''?></div>
                                    <div class="options-variation">
                                        <?php 
                                            if(isset($variation->options) && $variation->options): 
                                            foreach($variation->options as $option) :
                                        ?>
                                        <button><?=$option?></button>
                                        <?php endforeach; endif; ?>
                                    </div>
                                </div>
                            <?php endforeach; endif; ?>
                        </div>
                        <div class="shop-location">
                            <?php if(isset($shop_detail) && $shop_detail): ?>
                                <div class="mb-10">✅ Sản phẩm có sẵn tại: </div>
                                <?php 
                                    //is_shopee_verified==1->shop yêu thích
                                    //show_official_shop_label==1->shop mall
                                    //show_official_shop_label==""&&is_shopee_verified==""->shop thường
                                    if($shop_detail->is_shopee_verified):
                                ?>
                                    <div class="horizontal-badge"><svg class="shopee-svg-icon icon-tick" enable-background="new 0 0 15 15" viewBox="0 0 15 15" x="0" y="0"><g><path d="m6.5 13.6c-.2 0-.5-.1-.7-.2l-5.5-4.8c-.4-.4-.5-1-.1-1.4s1-.5 1.4-.1l4.7 4 6.8-9.4c.3-.4.9-.5 1.4-.2.4.3.5 1 .2 1.4l-7.4 10.3c-.2.2-.4.4-.7.4 0 0 0 0-.1 0z"></path></g></svg> Yêu Thích</div>
                                <?php elseif($shop_detail->show_official_shop_label): ?>
                                    <img class="official-shop-new-badge" src="https://deo.shopeemobile.com/shopee/shopee-pcmall-live-sg/assets/60cc60bb2ede69dffde9a70d62f8015f.png">
                                <?php else: ?>
                                    Shop: 
                                <?php endif; ?>
                                <a href="<?=shop_url($shop_detail->account->username, $shop_detail->shopid)?>"><?= isset($shop_detail->account->username) ? $shop_detail->account->username : '' ?></a>
                                <div class="mb-10">Thuộc: <strong><?= isset($shop_detail->place) ? $shop_detail->place : '' ?></strong></div>
                            <?php else: ?>
                                <div class="mb-10"><strong><?=isset($product->shop_location) ? $product->shop_location : ''?></strong></div> 
                            <?php endif;?>
                        </div>
                        <div><a href="<?php echo affilate_link($product->name, $product->shopid, $product->itemid); ?>" rel="nofollow" target="_blank" class="btn btn-danger btn-buy-now text-uppercase">Đến nơi mua</a></div>
                        <div class="product_other" id="product_other">
                            <span><i class="glyphicon glyphicon-gift"></i> Giao hàng toàn quốc</span>
                            <span><i class="glyphicon glyphicon-usd"></i> Giao hàng thu tiền</span>
                            <span><i class="glyphicon glyphicon-certificate"></i> Giao hàng bởi shopee</span>
                            <span><i class="glyphicon glyphicon-thumbs-up"></i> Chất lượng, Uy tín</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="product-box">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-9" id="product-info">
                        <div id="product-spec" style="border:0;margin-top:0;">
                            <h2>Thông số sản phẩm: <?php echo name_shopee($product->name); ?></h2>
                            <table class="table table-striped table-hover table-bordered">
                                <tbody>
                                <?php if($product->attributes){ ?>
                                    <?php foreach($product->attributes as $attribute){ ?>
                                        <tr>
                                            <td class="bold"><?php echo $attribute->name; ?></td>
                                            <td><?php echo $attribute->value; ?></td>
                                        </tr>
                                    <?php } ?>
                                <?php } ?>

                                </tbody>
                            </table>
                        </div>
                        <div id="product-description">
                            <h2>Mô tả sản phẩm: <?php echo name_shopee($product->name); ?></h2>
                            <div>
                                <?= nl2br($product->description) ?>
                            </div>
                            <div>
                            <?php if(!empty($dupplicate_key)): ?>
                                <?php foreach($dupplicate_key as $q): ?>
                                    <a href="<?=search_url($q)?>"><?=$q?></a>; 
                                <?php endforeach; ?>
                            <?php endif; ?>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-3" id="product-related">
                        <h2>Sản phẩm tương tự</h2>
                        <div id="product_related_box">
                            <?php if(isset($product_relateds)): ?>
                                <?php foreach($product_relateds as $key=>$item) : if($key<3): ?>
                                    <?php if(strlen($item->image) < 32 && empty($item->price)): ?>
                                    <div class="item-box">
                                        <a style="display: inline-block;" href="<?= product_url($item->shopid, $item->itemid, $item->name) ?>" title="<?= $item->name ?>">
                                            <img src="<?=base_url('assets/img/default-image-select-buy.jpg')?>" alt="default-image-select-buy.jpg">
                                            <span class="item-title"><?= name_shopee($item->name) ?></span>
                                            <?php if(isset($item->discount) && $item->discount){ ?>
                                                <div style="text-decoration: none;color:red">(- <?= discount_shopee($item->discount); ?>)</div>
                                            <?php } ?>
                                            <div class="btn btn-detail">Xem chi tiết</div>
                                        </a>
                                    </div>
                                    <?php else : ?>
                                        <div class="item-box">
                                            <a style="display: inline-block;" href="<?= product_url($item->shopid, $item->itemid, $item->name) ?>" title="<?= name_shopee($item->name) ?>">
                                                <img src="<?= image_shopee($item->image, 'tn') ?>" alt="<?= name_shopee($item->name) ?>">
                                                <span class="item-title"><?= name_shopee($item->name) ?></span>
                                                <span class="item-price promotion"><?= price_shopee($item->price) ?>
                                                    <?php if(isset($item->discount) && $item->discount){ ?>
                                                        <span style="text-decoration: none;color:red">(- <?= discount_shopee($item->discount); ?>)</span>
                                                    <?php } ?>
                                                    <span class="price-old"><?= price_shopee($item->price_before_discount) ?></span>
                                                </span>
                                            </a>
                                        </div>
                                    <?php endif; ?>
                                <?php unset($product_relateds[$key]); endif; endforeach; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <div class="quan-tam">
                    <h2>Có thể bạn quan tâm</h2>
                    <div class="home-main-block row">
                        <?php $list_name = ''; if(isset($product_relateds) && $product_relateds) { ?>
                            <?php foreach($product_relateds as $key => $item_rel){ ?>
                                <?php
                                    $prefix = ($key > 0) ? '<span class="text-danger">; </span>' : "";
                                    $list_name .= $prefix.$item_rel->name; 
                                ?>
                                <?php echo view('front/itemlist', array('item' => $item_rel)) ?>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<script src="<?= base_url('/assets/js/lightslider.min.js')?>"></script>
<script type="text/javascript">
    $(document).ready(function() {
        var slider = $("#imageGallery").lightSlider({
            gallery:true,
            item:1,
            vertical:false,
            verticalHeight:295,
            vThumbWidth:50,
            thumbItem:8,
            thumbMargin:4,
            slideMargin:0
        });
    });
</script>
<style>.review-pro{margin-bottom:10px;}.review-pro span{padding-right: 5px;font-weight:bold;color:red;}</style>