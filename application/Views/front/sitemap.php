<sitemapindex xmlns="https://www.sitemaps.org/schemas/sitemap/0.9">
    <?php if(isset($data_cate) && $data_cate): ?>
        <?php foreach ($data_cate as $cate) : ?>
            <sitemap><loc><?php echo sitemap_url($cate['display_name'], $cate['catid']);  ?></loc></sitemap>
        <?php endforeach; ?>
    <?php endif; ?>
</sitemapindex>