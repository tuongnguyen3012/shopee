<rss version="2.0">
    <channel>
        <title><![CDATA[ <?=META_TITLE ? META_TITLE : 'Siêu KM - Thiên Đường Mua Sắm'?> ]]></title>
        <link>
        <![CDATA[ <?=base_url()?> ]]>
        </link>
        <description>
            <![CDATA[ <?= META_DESCRIPTION ? META_DESCRIPTION : 'Thảnh thơi mua sắm trực tuyến tại Shopee với hàng ngàn sản phẩm từ đồ điện tử, thời trang, đồ gia dụng cho đến thực phẩm... với giá cực kỳ ưu đãi cùng nhiều khuyến mãi hấp dẫn.
                    Thiên Đường Mua Sắm Trực Tuyến Shopee Với Hàng Ngàn Sản Phẩm Ưu Đãi Hấp Dẫn, Giao Hàng Nhanh Chóng, Chất Lượng Tin Cậy, Hài Lòng Khách Hàng, Đảo Bảo Giá Ưu Đãi Brands: Apple, Samsung, Philips, Infinix, New Look, Xiaomi, Huggies,..'?>
            ]]>
        </description>
        <?php if(isset($items) && $items) : ?>
            <?php foreach($items as $item) : ?>
                <item>
                    <title>
                        <![CDATA[
                        <?= $item->name ?>
                        ]]>
                    </title>
                    <link>
                    <![CDATA[ <?= product_url($item->shopid, $item->itemid, $item->name) ?> ]]>
                    </link>
                    <description>
                        <![CDATA[
                        <?= $item->name.' Giá Chỉ '.price_shopee($item->price).' giảm '.$item->discount ?>
                        ]]>
                    </description>
                </item>
            <?php endforeach; ?>
        <?php endif; ?>
    </channel>
</rss>