<?php

// Location to the Paths config file.
// This should be the only line you need to
// edit in this file.
$pathsPath = '../application/Config/Paths.php';

// Path to the front controller (this file)
define('FCPATH', __DIR__.DIRECTORY_SEPARATOR);
if (! empty($_SERVER['HTTPS']))
{
    define('APP_BASE_URL', 'https://'.$_SERVER['HTTP_HOST'].'/');
}
else
{
    define('APP_BASE_URL', 'http://'.$_SERVER['HTTP_HOST'].'/');
}
define('HTTP_HOST_APP', $_SERVER['HTTP_HOST']);
/*
 *---------------------------------------------------------------
 * BOOTSTRAP THE APPLICATION
 *---------------------------------------------------------------
 * This process sets up the path constants, loads and registers
 * our autoloader, along with Composer's, loads our constants
 * and fires up an environment-specific bootstrapping.
 */

// Ensure the current directory is pointing to the front controller's directory
chdir(__DIR__);

// Load our paths config file
require $pathsPath;
$paths = new Config\Paths();

$app = require rtrim($paths->systemDirectory,DIRECTORY_SEPARATOR) . '/bootstrap.php';


$config_file_name = WRITEPATH.'data/'.$_SERVER['HTTP_HOST'].'/config.json';
$prefix_product = 'san-pham';
$prefix_category = 'danh-muc';
$prefix_search = 'mat-hang';
$prefix_shop = 'cua-hang';
$meta_title = 'Mua hàng với giá rẻ nhất đến từ shopee với các Khuyến mãi, mã giảm giá liên tục';
$meta_description = 'Các sản phẩm tốt nhất với giá rẻ nhất đến từ shopee với các Khuyến mãi, mã giảm giá và hoàn toàn được đảm bảo bởi shopee, nhận ship toàn quốc - mua hàng ngay';
$config_header = '';
$config_footer = '';
if(file_exists($config_file_name))
{
    $configs = @json_decode(file_get_contents($config_file_name), true);
    $prefix_product = (isset($configs['prefix_product']) && !empty($configs['prefix_product'])) ? $configs['prefix_product'] : 'san-pham';
    $prefix_category = (isset($configs['prefix_category']) && !empty($configs['prefix_category'])) ? $configs['prefix_category'] : 'danh-muc';
    $prefix_search = (isset($configs['prefix_search']) && !empty($configs['prefix_search'])) ? $configs['prefix_search'] : 'mat-hang';
    $prefix_shop = (isset($configs['prefix_shop']) && !empty($configs['prefix_shop'])) ? $configs['prefix_shop'] : 'cua-hang';
    if(isset($configs['meta_title']) && !empty($configs['meta_title'])){
        $meta_title = $configs['meta_title'];
    }
    if(isset($configs['meta_description']) && !empty($configs['meta_description'])){
        $meta_description = $configs['meta_description'];
    }
    $config_header = isset($configs['header']) ? $configs['header'] : '';
    $config_footer = isset($configs['footer']) ? $configs['footer'] :'';
}
define('PREFIX_PRODUCT', $prefix_product);
define('PREFIX_CATEGORY', $prefix_category);
define('PREFIX_SEARCH', $prefix_search);
define('PREFIX_SHOP', $prefix_shop);
define('META_TITLE', $meta_title);
define('META_DESCRIPTION', $meta_description);
define('CONFIG_HEADER', $config_header);
define('CONFIG_FOOTER', $config_footer);

/*
 *---------------------------------------------------------------
 * LAUNCH THE APPLICATION
 *---------------------------------------------------------------
 * Now that everything is setup, it's time to actually fire
 * up the engines and make this app do it's thang.
 */
$app->run();
